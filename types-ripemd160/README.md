# Installation
> `npm install --save @types/ripemd160`

# Summary
This package contains type definitions for ripemd160 (https://github.com/crypto-browserify/ripemd160#readme).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/ripemd160

Additional Details
 * Last updated: Wed, 05 Dec 2018 02:47:39 GMT
 * Dependencies: @types/node
 * Global values: none

# Credits
These definitions were written by BendingBender <https://github.com/BendingBender>.
